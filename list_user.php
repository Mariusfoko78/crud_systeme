<?php
require_once 'connect.php';

$db = getDBConnection(); // Connexion à la base de données

// Supprimer un étudiant s'il y a une demande de suppression
if(isset($_GET['action']) && $_GET['action'] == 'delete' && isset($_GET['id'])) {
    $id = $_GET['id'];
    $stmt = $db->prepare("DELETE FROM etudiants WHERE id = ?");
    if($stmt->execute([$id])) {
        $success_message = "Étudiant supprimé avec succès.";
    } else {
        $error_message = "Erreur lors de la suppression de l'étudiant.";
    }
}

// Récupérer tous les étudiants ou effectuer une recherche
$search = isset($_GET['search']) ? $_GET['search'] : '';
if ($search) {
    $stmt = $db->prepare("SELECT * FROM etudiants WHERE nom LIKE ?");
    $stmt->execute([$search . '%']);
} else {
    $stmt = $db->prepare("SELECT * FROM etudiants");
    $stmt->execute();
}
$etudiants = $stmt->fetchAll();
?>



<div class="container mt-5">
    <?php if(isset($success_message)): ?>
        <div class="alert alert-success"><?= $success_message ?></div>
    <?php endif; ?>
    <?php if(isset($error_message)): ?>
        <div class="alert alert-danger"><?= $error_message ?></div>
    <?php endif; ?>

    <h1 class="mb-5">Liste des Étudiants</h1>

    <table class="table table-bordered">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Avatar</th>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Matricule</th>
                <th scope="col">Date de Naissance</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($etudiants as $etudiant): ?>
                <tr>
                    <td>
                        <?php if ($etudiant['avatar_path']): ?>
                            <img src="uploads/<?= htmlspecialchars($etudiant['avatar_path']) ?>" alt="Avatar de <?= htmlspecialchars($etudiant['nom']) ?>" width="40">
                        <?php else: ?>
                            <img src="default-avatar.png" alt="Avatar par défaut" width="50">
                        <?php endif; ?>
                    </td>
                    <td><?= htmlspecialchars($etudiant['nom']) ?></td>
                    <td><?= htmlspecialchars($etudiant['prenom']) ?></td>
                    <td><?= htmlspecialchars($etudiant['matricule']) ?></td>
                    <td><?= htmlspecialchars($etudiant['date_de_naissance']) ?></td>
                    <td>
                        <a class="btn btn-success btn-sm" href="index.php?page=edit&id=<?= $etudiant['id'] ?>">Edit</a>
                        <a class="btn btn-danger btn-sm" href="index.php?action=delete&id=<?= $etudiant['id'] ?>" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cet étudiant ?')">Delete</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>


