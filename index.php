<?php
session_start();
require_once 'config.php';
require_once 'connect.php';
include_once 'header.php';




// Déterminer la page à inclure
$page = isset($_GET['page']) ? $_GET['page'] : 'list';

// Liste des pages valides
$validPages = ['list', 'add', 'edit', 'search'];

// Si la page demandée est valide, l'inclure, sinon inclure la page 'list'
if (in_array($page, $validPages)) {
    $pageFile = $page . '_user.php';
    if (file_exists($pageFile)) {
        include_once $pageFile;
    } else {
        echo "<p>Erreur : la page demandée n'existe pas.</p>";
    }
} else {
    include_once 'list_user.php';
}

include_once 'footer.php';
?>
