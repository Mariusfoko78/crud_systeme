<?php
require_once 'connect.php';
require_once 'requete.php';

// Vérifiez si l'identifiant de l'étudiant est présent dans l'URL
if (isset($_GET['id'])) {
    $id = intval($_GET['id']);

    // Récupérez les informations de l'étudiant à partir de la base de données
    $etudiant = getStudentById($id);
    $error_message = ''; // Initialisez la variable $error_message
    $success_message = ''; // Initialisez la variable $success_message


    // Vérifiez si l'étudiant existe
    if ($etudiant) {
        // Vérifiez si le formulaire a été soumis
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Récupérez les données du formulaire
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $date_de_naissance = $_POST['date_de_naissance'];

            // Obtenez le matricule de l'étudiant
            $matricule = $etudiant->matricule;

            // Validez les données du formulaire
            if (!validateName($nom)) {
                $error_message = "Le nom n'est pas valide.";
            } elseif (!validateName($prenom)) {
                $error_message = "Le prénom n'est pas valide.";
            } elseif (!validateDateNaissance($date_de_naissance)) {
                $error_message = "La date de naissance n'est pas valide.";
            } else {
                // Mettez à jour l'étudiant dans la base de données
                $result = updateStudent($nom, $prenom, $matricule, $date_de_naissance);

                // Vérifiez si la mise à jour a été réussie
                if ($result) {
                    $success_message = "Étudiant mis à jour avec succès.";
                } else {
                    $error_message = "Erreur lors de la mise à jour de l'étudiant.";
                }
            }
        }
    } else {
        $error_message = "Étudiant non trouvé.";
    }
} else {
    $error_message = "ID d'étudiant manquant.";
}

?>



<div class="container mt-5">
    <?php if ($error_message): ?>
        <div class="alert alert-danger"><?= $error_message ?></div>
    <?php endif; ?>
    <?php if ($success_message): ?>
        <div class="alert alert-success"><?= $success_message ?></div>
    <?php endif; ?>

    <?php if (isset($etudiant)): ?>
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="nom">Nom :</label>
                <input type="text" class="form-control" id="nom" name="nom" value="<?= isset($etudiant->nom) ? htmlspecialchars($etudiant->nom) : '' ?>" required maxlength="25">
            </div>
            <div class="form-group">
                <label for="prenom">Prénom :</label>
                <input type="text" class="form-control" id="prenom" name="prenom" value="<?= isset($etudiant->prenom) ? htmlspecialchars($etudiant->prenom) : '' ?>" required maxlength="25">
            </div>
            <div class="form-group">
                <label for="date_de_naissance">Date de naissance :</label>
                <input type="date" class="form-control" id="date_de_naissance" name="date_de_naissance" value="<?= isset($etudiant->date_de_naissance) ? htmlspecialchars($etudiant->date_de_naissance) : '' ?>" required>
            </div>
            <div class="form-group">
                <label for="avatar">Avatar :</label>
                <input type="file" class="form-control-file" id="avatar" name="avatar">
            </div>
            <button type="submit" class="btn btn-primary">Mettre à jour</button>
        </form>
    <?php endif; ?>
</div>









