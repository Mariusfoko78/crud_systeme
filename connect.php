<?php
require_once 'config.php';

// Connexion à la base de données
/**
 * @param string $dbname
 * @param string $user
 * @param string $password
 * @param string $host
 * @return PDO
 */
function getDBConnection(): PDO {
    return new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST
        , DB_USER, DB_PASSWORD);
}
