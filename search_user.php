<?php

require_once 'connect.php';

if (isset($_GET['search'])) {
    $search = $_GET['search'];

    // Connexion à la base de données
    $db = getDBConnection();
    $query = "SELECT nom, prenom, date_de_naissance, avatar_path FROM etudiants WHERE nom LIKE :search";
    $stmt = $db->prepare($query);
    $stmt->execute(['search' => $search . '%']);
    $result = $stmt->fetchAll();

    // Affichage des résultats de la recherche
    if ($result && count($result) > 0) {
        echo "<h2>Résultats de la recherche pour '$search'</h2>";
        echo "<div class='table-responsive'>";
        echo "<table class='table'>";
        echo "<thead><tr><th scope='col'>Avatar</th><th scope='col'>Nom</th><th scope='col'>Prénom</th><th scope='col'>Date de naissance</th></tr></thead>";
        echo "<tbody>";
        foreach ($result as $row) {
            echo "<tr>";
            echo "<td><img src='uploads/" . htmlspecialchars($row['avatar_path']) . "' alt='Avatar' style='max-width: 100px;'></td>";
            echo "<td>" . htmlspecialchars($row['nom']) . "</td>";
            echo "<td>" . htmlspecialchars($row['prenom']) . "</td>";
            echo "<td>" . htmlspecialchars($row['date_de_naissance']) . "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "<p>Aucun étudiant trouvé pour '$search'.</p>";
    }
}
?>



















