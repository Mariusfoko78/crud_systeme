<?php

require_once 'connect.php'; // Inclure le fichier de connexion à la base de données

// Vérifie si une recherche est effectuée
if (isset($_GET['search'])) {
    $search = $_GET['search'];

    // connexion a la db 
    $db = connect();
    $query = "SELECT nom FROM utilisateurs WHERE nom LIKE '$search%'";
    $result = $db->query($query);

    // Affichage des résultats de la recherche
    if ($result && $result->rowCount() > 0) {
        echo "<h2>Résultats de la recherche pour '$search'</h2>";
        echo "<ul>";
        while ($row = $result->fetchObject()) {
            echo "<li>" . htmlspecialchars($row->nom) . "</li>";
        }
        echo "</ul>";
    } else {
        echo "<p>Aucun utilisateur trouvé pour '$search'.</p>";
    }
}
?>



<!-- Liste des utilisateurs -->
<div class="user-list">
    <h2>Liste des utilisateurs</h2>
    <?php
    // Récupérer tous les utilisateurs
    // connexion  ala db 
    $db = connect();
    $query_all_users = "SELECT nom FROM utilisateurs";
    $result_all_users = $db->query($query_all_users);
    
    // Affichage de la liste des utilisateurs
    echo "<ul>";
    while ($row = $result_all_users->fetchObject()) {
        echo "<li>" . htmlspecialchars($row->nom) . "</li>";
    }
    echo "</ul>";
    ?>
</div>