<?php
require_once 'connect.php';
require_once 'requete.php';


// Traitement du formulaire
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $nom = trim($_POST['nom']);
    $prenom = trim($_POST['prenom']);
    $matricule = trim($_POST['matricule']);
    $dateNaissance = trim($_POST['date_de_naissance']);

    $errors = [];

    if (!validateName($nom)) {
        $errors[] = "Le nom n'est pas valide.";
    }

    if (!validateName($prenom)) {
        $errors[] = "Le prénom n'est pas valide.";
    }

    if (!validateMatricule($matricule)) {
        $errors[] = "Le matricule doit être une série de 4 chiffres.";
    }

    if (!validateDateNaissance($dateNaissance)) {
        $errors[] = "L'étudiant doit avoir au moins 18 ans et la date de naissance ne peut pas être dans le futur.";
    }

    if (empty($errors)) {
        if (matriculeExists($matricule)) {
            $errors[] = "Le matricule $matricule est déjà utilisé.";
        } else {
            $avatarNewName = handleAvatar($_FILES['avatar']);

            if ($avatarNewName === null) {
                $errors[] = "Erreur lors du téléchargement du fichier.";
            } else {
                try {
                      $db = getDBConnection(); // Connexion à la base de données

                    $stmt = $db->prepare("INSERT INTO etudiants (nom, prenom, matricule, date_de_naissance, avatar_path) VALUES (:nom, :prenom, :matricule, :date_de_naissance, :avatar_path)");
                    if ($stmt->execute([':nom' => $nom, ':prenom' => $prenom, ':matricule' => $matricule, ':date_de_naissance' => $dateNaissance, ':avatar_path' => $avatarNewName
                    ])) {
                        $success_message = "Étudiant ajouté avec succès.";
                        header("Location: index.php?page=list");
                    exit; 

                    } else {
                        $errors[] = "Erreur lors de l'ajout de l'étudiant.";
                    }

                } catch (PDOException $e) {
                    $errors[] = "Erreur de connexion à la base de données.";
                }
            }
        }
    }
}
?>


<form action="" method="post" enctype="multipart/form-data" class="container mt-4">
    <div class="mb-3">
        <label for="nom" class="form-label">Nom :</label>
        <input type="text" id="nom" name="nom" class="form-control"  required maxlength="25">
    </div> 

    <div class="mb-3">
        <label for="prenom" class="form-label">Prénom :</label> 
        <input type="text" id="prenom" name="prenom" class="form-control" maxlength="25"  required >
    </div>

    <div class="mb-3">
        <label for="matricule" class="form-label">Matricule (4 chiffres) :</label>
        <input type="text" id="matricule" name="matricule" class="form-control" required pattern="\d{4}" maxlength="4">
    </div>

    <div class="mb-3">
        <label for="date_de_naissance" class="form-label">Date de naissance :</label>
        <input type="date" id="date_de_naissance" name="date_de_naissance" class="form-control" required>
    </div>

    <div class="mb-3">
        <label for="avatar" class="form-label">Avatar (JPEG ou PNG uniquement, taille maximale 5 Mo) :</label>
        <input type="file" id="avatar" name="avatar" class="form-control" accept=".jpg, .jpeg, .png" required>
    </div>

    <button type="submit" class="btn btn-primary">Inscrire</button>
</form>


