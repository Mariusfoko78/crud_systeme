<?php

require_once 'config.php';
require_once 'connect.php';


// Fonction pour valider le nom et le prénom
function validateName($name): bool
{
    $pattern = '/^[A-Za-zÀ-ÖØ-öø-ÿ\s-]+$/';
    return preg_match($pattern, $name) && strlen($name) <= 255;
}

// Fonction pour valider le matricule
function validateMatricule($matricule): bool|int
{
    $pattern = '/^\d{4}$/';
    return preg_match($pattern, $matricule);
}

// Fonction pour valider la date de naissance
function validateDateNaissance($dateNaissance): bool
{
    if (empty($dateNaissance)) {
        return false;
    }

    $dateNow = new DateTime();
    $dateBirth = new DateTime($dateNaissance);
    $age = $dateNow->diff($dateBirth)->y;

    return $age >= 18 && $dateBirth <= $dateNow;
}

// Fonction pour vérifier si le matricule existe déjà
function matriculeExists($matricule): bool
{
    try {
         $db = getDBConnection(); // Connexion à la base de données

        $stmt = $db->prepare("SELECT COUNT(*) FROM etudiants WHERE matricule = ?");
        $stmt->execute([$matricule]);
        $count = $stmt->fetchColumn();
        return $count > 0;
    } catch (PDOException $e) {
        return true; // Erreur de connexion à la base de données, supposer que le matricule existe
    }
}

/**
 * Fonction pour traiter l'avatar
 */
function handleAvatar($avatar): ?string
{
    if (!isset($avatar) || $avatar['error'] != 0) {
        return null;
    }

    $avatarName = $avatar['name'];
    $avatarExt = strtolower(pathinfo($avatarName, PATHINFO_EXTENSION));
    $allowedExtensions = ['jpg', 'jpeg', 'png'];

    if (!in_array($avatarExt, $allowedExtensions)) {
        return null;
    }

    if ($avatar['size'] > 5000000) {
        return null;
    }

    $avatarNewName = uniqid('', true) . '.' . $avatarExt;
    $avatarDestination = 'uploads/' . $avatarNewName;

    if (!move_uploaded_file($avatar['tmp_name'], $avatarDestination)) {
        return null;
    }

    return $avatarNewName;
}

/**
 * Fonction pour rechercher des étudiants par nom.
 *
 * @param string $search Le terme de recherche.
 * @return array Un tableau d'objets contenant les résultats de la recherche.
 */
function searchStudents(string $search): array
{
    $db = getDBConnection();
    $query = "SELECT * FROM etudiants WHERE nom LIKE :search";
    $stmt = $db->prepare($query);
    $stmt->execute(['search' => $search . '%']);
    return $stmt->fetchAll(PDO::FETCH_OBJ);
}

/**
 * Fonction pour récupérer tous les étudiants.
 *
 * @return array Un tableau d'objets contenant tous les étudiants.
 */
function getAllStudents() {
    $db = getDBConnection();
    $query = "SELECT * FROM etudiants";
    $stmt = $db->query($query);
    return $stmt->fetchAll(PDO::FETCH_OBJ);
}

/**
 * Fonction pour ajouter un étudiant.
 *
 * @param string $nom Le nom de l'étudiant.
 * @param string $prenom Le prénom de l'étudiant.
 * @param string $matricule Le matricule de l'étudiant.
 * @param string $date_de_naissance La date de naissance de l'étudiant.
 * @return bool TRUE en cas de succès, FALSE sinon.
 */
function addStudent(string $nom, string $prenom, string $matricule, string $date_de_naissance) {
    $db = getDBConnection();
    $query = "INSERT INTO etudiants (nom, prenom, matricule, date_de_naissance) VALUES (:nom, :prenom, :matricule, :date_de_naissance)";
    $stmt = $db->prepare($query);
    return $stmt->execute([
        ':nom' => $nom,
        ':prenom' => $prenom,
        ':matricule' => $matricule,
        ':date_de_naissance' => $date_de_naissance
    ]);
}

/**
 * Fonction pour vérifier si un matricule est déjà utilisé.
 *
 * @param string $matricule Le matricule à vérifier.
 * @return bool TRUE si le matricule existe, FALSE sinon.
 */
function isMatriculeExists(string $matricule) {
    $db = getDBConnection();
    $query = "SELECT COUNT(*) FROM etudiants WHERE matricule = :matricule";
    $stmt = $db->prepare($query);
    $stmt->execute([':matricule' => $matricule]);
    return $stmt->fetchColumn() > 0;
}



/**
 * Fonction pour récupérer un étudiant par son ID.
 *
 * @param int $id L'identifiant de l'étudiant.
 * @return mixed|array|null Les informations de l'étudiant s'il est trouvé, sinon NULL.
 */
function getStudentById(int $id): mixed
{
    $db = getDBConnection();
    $query = "SELECT * FROM etudiants WHERE id = :id";
    $stmt = $db->prepare($query);
    $stmt->execute([':id' => $id]);
    return $stmt->fetchObject();
}

/**
 * Fonction pour supprimere un etudiant
 * 
 * 
 */
function deleteStudent($matricule) {
    $db = getDBConnection();
    $query = "DELETE FROM etudiants WHERE matricule = :matricule";
    $stmt = $db->prepare($query);
    $stmt->execute([':matricule' => $matricule]);
}


/**
 * fonction pour la misea a jour  de l'etudiant
 *
 */
function updateStudent($nom, $prenom, $matricule, $date_de_naissance): bool
{
    try {
        $db = getDBConnection();
        $query = "UPDATE etudiants SET nom = :nom, prenom = :prenom, date_de_naissance = :date_de_naissance WHERE matricule = :matricule";
        $stmt = $db->prepare($query);
        $stmt->execute([
            ':nom' => $nom,
            ':prenom' => $prenom,
            ':date_de_naissance' => $date_de_naissance,
            ':matricule' => $matricule
        ]);
        return true; // Succès de la mise à jour
    } catch (PDOException $e) {
        // Gérer l'erreur
        error_log("Erreur lors de la mise à jour de l'étudiant : " . $e->getMessage());
        return false; // Échec de la mise à jour
    }
}



/**
 * Fonction pour récupérer un étudiant par son matricule.
 *
 * @param int $matricule Le matricule de l'étudiant.
 * @return mixed Les informations de l'étudiant ou NULL s'il n'est pas trouvé.
 */

 function getStudentByMatricule($matricule) {
    $db = getDBConnection();
    $query = "SELECT * FROM etudiants WHERE matricule = :matricule";
    $stmt = $db->prepare($query);
    $stmt->execute([':matricule' => $matricule]);
    return $stmt->fetchObject();
}


